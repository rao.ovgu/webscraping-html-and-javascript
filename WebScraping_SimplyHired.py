# -*- coding: utf-8 -*-
"""
Created on Thu Jan 23 22:49:49 2020

@author: rajatha
"""

# -*- coding: utf-8 -*-
"""
Created on Thu Jan 23 19:56:32 2020

@author: rajatha
"""

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import Firefox
#from selenium.webdriver.support.select import Select
from selenium.webdriver.common.keys import Keys
#dropdown, list
#keyboard ops on driver
#from selenium.webdriver.common.action_chains import ActionChains
import time
from bs4 import BeautifulSoup
import pandas as pd
import re
import requests
import os
import glob

def scraper(search_term, output_file_name):
       ##to open the link
    path = "P:\\geckodriver-v0.26.0-win64\\geckodriver.exe"
    driver = Firefox(executable_path=path)
    url_base = "https://www.simplyhired.com/"
    driver.set_page_load_timeout(100)
    driver.get(url_base)
    driver.maximize_window()
    
    keyword = search_term
    #enter required information in base url
    driver.find_element_by_name("q").send_keys(keyword)
    driver.find_element_by_name("q").send_keys(Keys.ENTER)
    #time.sleep(30) #to overcome StaleElementReferenceException
    wait = WebDriverWait(driver,10)
    
    titles = []
    
    next = 1
    while next < 10 :
        #1.get the link of corresponding jobs
        elems = driver.find_elements_by_xpath("//a[@class='card-link']")
        for elem in elems:
            titles.append(elem.get_attribute("href"))
            #remove none values
            titles = list(filter(None, titles))
        time.sleep(5)
        #not working driver.execute_script("window.scrollTo(0, 1080)") 
        driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
        ###to get the href of next page
        #nextpg = driver.find_element_by_xpath("//a[@class='next-pagination' and @aria-label='Next page']")
        wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'next-pagination')))
        #this wait depends on webdriverwait
        annual_link = driver.find_element_by_class_name('next-pagination')
        annual_link.click()
        print("nextpage")  
        next+= 1
    
    print(len(titles))
    #convert to json
    #titles = pd.DataFrame(titles)
    #print(titles)
    #titles.to_csv(titles)
    # Create a dictionary
    d = {'key':'value'}
    print(d)
    
    
    # Update the dictionary
    d['new key'] = 'new value'
    print(d)
    
    
    job_no = 0
    job_description = {} #sal and job description
    
    #parse collected href with BS
    for title in titles:
        response = requests.get(title)#each job href
        data = response.text
        job_soup = BeautifulSoup(data,'html.parser') #to parse tags
        job_d = job_soup.find("div",{"class":"viewjob-description ViewJob-description"}).text
        job_d = job_d.replace("\n","")
        sal_str = job_soup.find("div",{"class":"extra-info"}).text
        print(sal_str)
        sal_str = sal_str.replace(",","")
        #sal_str = sal_str.replace("re.findall(r'\d+[ ][d][a][y]', sal_str)","")
        print(sal_str)
        sal_eng = re.findall(r'\d{2,6}', sal_str)
        #check the salary details
        
            
        
        print(len(sal_eng))
        print(sal_eng)
        print(type(sal_str))
        
        ##post 10 days ago, sal/hour etc
        #print(job_no)
        #can break if salary not specified as a range!
        #if isinstance(sal_eng,list):
        if len(sal_eng) > 1:   
            if "a week" in sal_str:
                first = int(sal_eng[0])
                #1st value may be time since last post
                if first < 100:
                   first = int(sal_eng[1]) 
                   second = int(sal_eng[2])
                else:
                    second = int(sal_eng[1])
                sal = int((first + second)/2)
                sal = int(sal*52)
               
                    
            elif "an hour" in sal_str:
                first = int(sal_eng[0])
                if first < 20:
                   first = int(sal_eng[1]) 
                   second = int(sal_eng[2])
                else:
                    second = int(sal_eng[1])
                sal = int((first + second)/2)
                sal = int(sal*2080) 
                
            elif "a month" in sal_str:
                first = int(sal_eng[0])
                if first < 1000:
                   first = int(sal_eng[1]) 
                   second = int(sal_eng[2])
                else:
                    second = int(sal_eng[1])
                sal = int((first + second)/2)
                sal = int(sal*12)
                
            elif "year" in sal_str:
                first = int(sal_eng[0])
                if first < 10000:
                   first = int(sal_eng[1]) 
                   second = int(sal_eng[2])
                else:
                    second = int(sal_eng[1])
                sal = int((first + second)/2)
               
            print(sal)    
            normalizedsal = float(sal/1000000)
        
        elif len(sal_eng) == 1:
             
            sal=int(sal_eng[0])
            if "a week" in sal_str:
                sal = int(sal*52)
            elif "an hour" in sal_str:
                sal = int(sal*2080) 
            elif "a month" in sal_str:
                sal = int(sal*12)
            print(sal)    
            normalizedsal = float(sal/1000000)
            
        else:
            sal = 'n/a'
            print(sal) 
            normalizedsal = 'n/a'
        job_no+=1
        job_description[job_no] = [job_d, sal, normalizedsal]
        
       
    job_description_df = pd.DataFrame.from_dict(job_description, orient = 'index', columns = ['Job Description','Salary','Normalised Salary']) #convert dictionary to df  
    #job_description_df.head()  #print
    file_path = "P:\\HiWi projects\\WebscrapingOutputs\\" + output_file_name
    print(file_path)
    job_description_df.to_csv(file_path)
  
    driver.back() #to naviagate back to original page
        
        
scraper("engineer","engineering_Jobs.csv")
scraper("manager","manager_Jobs.csv")
scraper("Human Resource","Human_Resource_Jobs.csv")
scraper("Manufacturing Director","Manufacturing_Director_Jobs.csv")
scraper("Scientist","Scientist_Jobs.csv")
scraper("Sales Executive","Sales_Executive_Jobs.csv")
scraper("Senior Engineer","Senior_Engineer_Jobs.csv")


os.chdir("P:\\HiWi projects\\WebscrapingOutputs\\")
extension = 'csv'
all_filenames = [i for i in glob.glob('*.{}'.format(extension))]
#combine all files in the list
combined_csv = pd.concat([pd.read_csv(f) for f in all_filenames ])
#export to csv
combined_csv.to_csv( "combined_csv.csv", index=False, encoding='utf-8-sig')
#remove repeated indexes of all the combined files
df_new_idx = pd.read_csv('combined_csv.csv').drop(['Unnamed: 0'],axis=1)
df_new_idx['id']=df_new_idx.index
df_new_idx.to_csv('combinedResults_csv.csv')
