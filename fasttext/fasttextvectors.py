import pandas as pd
import numpy as np
from fasttext import load_model
from nltk.corpus import stopwords
stop = stopwords.words('english')

data_scraped_df = pd.read_csv("P:\\HiWi projects\\WebscrapingOutputs\\scraped_csv.csv", index_col = "id_scrape")
data_scraped_df['JDscraped_without_stopwords'] = data_scraped_df['Job Description'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stop)]))


data_dataset_df = pd.read_csv("P:\\HiWi projects\\Scrape\\job_Dataset\\dataset\\Employees_new.csv", index_col = "id_dataset")
data_dataset_df['JDdataset_without_stopwords'] = data_dataset_df['JobDescription'].apply(lambda x: ' '.join([word for word in x.split() if word not in (stop)]))

ft_model = load_model("P:\\HiWi projects\\wiki.en\\wiki.en.bin")

vecscrape = np.empty(300)
vecds = list()
for jd in data_scraped_df['JDscraped_without_stopwords']:
    vec = np.zeros(300)
    l = len(jd)
    jd = jd.split()
    for i in jd:
        vec = np.add(vec,ft_model.get_word_vector(i))
    
    vec = (np.divide(vec,l))
    np.concatenate((vecscrape,vec))
    #
    vecds.append(vec)
    data_scraped_df['AvgVec_Scrape'] =  pd.DataFrame(vecscrape)
    data_scraped_df.to_csv("P:\\HiWi projects\\vecscrape_new.csv")
print(vecds)
