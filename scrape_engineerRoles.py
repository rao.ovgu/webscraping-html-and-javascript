from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver import Firefox
#from selenium.webdriver.support.select import Select
from selenium.webdriver.common.keys import Keys
#dropdown, list
#keyboard ops on driver
#from selenium.webdriver.common.action_chains import ActionChains
import time
from bs4 import BeautifulSoup
import pandas as pd
import json
import re
import requests

##to open the link
path = "P:\\geckodriver-v0.26.0-win64\\geckodriver.exe"
driver = Firefox(executable_path=path)
url_base = "https://www.simplyhired.com/"
driver.set_page_load_timeout(100)
driver.get(url_base)
driver.maximize_window()

keyword = "manager"
#enter required information in base url
driver.find_element_by_name("q").send_keys(keyword)
driver.find_element_by_name("q").send_keys(Keys.ENTER)
#time.sleep(30) #to overcome StaleElementReferenceException
wait = WebDriverWait(driver,10)

engineer_titles = []

next = 0
while next < 5 :
    #1.get the link of corresponding jobs
    elems = driver.find_elements_by_xpath("//a[@class='card-link']")
    for elem in elems:
        engineer_titles.append(elem.get_attribute("href"))
        #remove none values
        engineer_titles = list(filter(None, engineer_titles))
    time.sleep(5)
    #not working driver.execute_script("window.scrollTo(0, 1080)") 
    driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")
    ###to get the href of next page
    nextpg = driver.find_element_by_xpath("//a[@class='next-pagination' and @aria-label='Next page']")
    wait.until(EC.element_to_be_clickable((By.CLASS_NAME, 'next-pagination')))
    #this wait depends on webdriverwait
    annual_link = driver.find_element_by_class_name('next-pagination')
    annual_link.click()
    print("nextpage")  
    next+= 1

print(len(engineer_titles))
#convert to json
#engineer_titles = pd.DataFrame(engineer_titles)
#print(engineer_titles)
#engineer_titles.to_csv(engineer_titles)
# Create a dictionary
d = {'key':'value'}
print(d)


# Update the dictionary
d['new key'] = 'new value'
print(d)


job_no = 0
job_d_engineer = {} #sal and job description

#parse collected href with BS
for engineer_title in engineer_titles:
    response = requests.get(engineer_title)#each job href
    data = response.text
    job_soup = BeautifulSoup(data,'html.parser') #to parse tags
    job_d = job_soup.find("div",{"class":"viewjob-description ViewJob-description"}).text
    job_d = job_d.replace("\n","")
    sal_eng = job_soup.find("div",{"class":"extra-info"}).text
    sal_eng = sal_eng.replace(",","")
   
    sal_eng = re.findall(r'\d+', sal_eng)
    #print(job_no)
    #can break if salary not specified as a range!
    if isinstance(sal_eng,list):
        first = int(sal_eng[0])
        second = int(sal_eng[1])
        sal = int((first + second)/2)
        normalizedsal = float(sal/1000000)
    else:
       sal=int(sal_eng)
       normalizedsal = float(sal/1000000)
    job_no+=1
    job_d_engineer[job_no] = [job_d, sal, normalizedsal]
    
   
job_d_engineer_df = pd.DataFrame.from_dict(job_d_engineer, orient = 'index', columns = ['Job Description','Salary','Normalised Salary']) #convert dictionary to df  
job_d_engineer_df.head()  #print
job_d_engineer_df.to_csv('engineeringjobs.csv')
     
     
     
driver.back() #to naviagate back to original page