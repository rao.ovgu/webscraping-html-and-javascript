# -*- coding: utf-8 -*-
"""
Created on Fri Dec 13 00:38:37 2019

@author: rajatha
"""

from bs4 import BeautifulSoup
import requests
import pandas as pd

url = "https://boston.craigslist.org/search/npo"
response = requests.get(url)
print(response)

data = response.text

#print(data)

soup = BeautifulSoup(data,'html.parser') #to parse tags

#extracting all tags and links is href from 1 webpage, clean links from page
tags = soup.find_all('a')

for tag in tags :
    print(tag.get('href'))
    

#titles = soup.find_all("a",{"class":"result-title"})
#
#for title in titles:     #job titles
#    print(title.text)
#    
#    
#addresses = soup.find_all("span",{"class":"result-hood"})  #address is in span tag, class = result hood
#
#for address in addresses:
#    print(address.text)
    
#extracting all info from a wrapper class, to avoid missing values
    


#for job in jobInfo:   #find first occurrence
#    title = job.find("a",{"class":"result-title"}).text
#    location_tag = job.find("span",{"class":"result-hood"})
#    location = location_tag.text[2:-1] if  location_tag else "N/A"
#    date = job.find("time",{"class":"result-date"}).text
#    link = job.find("a",{"class":"result-title"}).get('href')
#    
#    #connect to each link and scrape
#    
#    job_response = requests.get('link')
#    job_data = job_response.text  #link data
#    job_soup = BeautifulSoup(job_data,'html.parser') #sourcse code
#    job_description = job_soup.find('section',{'id':'postingbody'}).text  #job description extraction
#    job_attributes_tag = job_soup.find('p',{'class':'attrgroup'}).text  #some job attributes
#    job_attributes = job_attributes_tag.text if job_attributes_tag else 'NA'
#    
#    print('Job Title:', title, '\nLocation:', location, '\nDate:', date, '\nLink:', link,"\n", job_attributes, '\nJob Description:', job_description,'\n---')
#    

# Create a dictionary
d = {'key':'value'}
print(d)


# Update the dictionary
d['new key'] = 'new value'
print(d)


npo_jobs = {}
jobs = soup.find_all("p",{"class":"result-info"})
job_no = 0
while True:
    
    response = requests.get(url)
    data = response.text
    soup = BeautifulSoup(data,'html.parser')
    jobs = soup.find_all('p',{'class':'result-info'})
    
    for job in jobs:
        title = job.find('a',{'class':'result-title'}).text
        location_tag = job.find('span',{'class':'result-hood'})
        location = location_tag.text[2:-1] if location_tag else "N/A"
        date = job.find('time', {'class': 'result-date'}).text
        #extract all hrefs from main page
        link = job.find('a', {'class': 'result-title'}).get('href')
        job_response = requests.get(link)
        job_data = job_response.text
        job_soup = BeautifulSoup(job_data, 'html.parser')
        job_description = job_soup.find('section',{'id':'postingbody'}).text
        job_attributes_tag = job_soup.find('p',{'class':'attrgroup'})
        job_attributes = job_attributes_tag.text if job_attributes_tag else "N/A"
        job_no+=1
        #npo_jobs[job_no] = [title, location, date, link, job_attributes, job_description] #list of job details
        #print('Job Title:', title, '\nLocation:', location, '\nDate:', date, '\nLink:', link,"\n", job_attributes, '\nJob Description:', job_description,'\n---')
    #extract next tag with relative url upto second last tag
        npo_jobs[job_no] = [job_description]
    url_tag = soup.find('a',{'title':'next page'})
    if url_tag.get('href'):   #if not empty
        url= 'https://boston.craigslist.org' + url_tag.get('href')
        print(url)
    else:
        break
        
print("Total Jobs:", job_no)


#writing output to json 
#npo_jobs_df = pd.DataFrame.from_dict(npo_jobs, orient = 'index', columns = ['Job Title','Location','Date', 'Link', 'Job Attributes', 'Job Description']) #convert dictionary to df  
npo_jobs_df = pd.DataFrame.from_dict(npo_jobs, orient = 'index', columns = ['Job Description']) #convert dictionary to df  
npo_jobs_df.head()  #print
npo_jobs_df.to_csv('npo_jobs.csv')

