# Webscraping - HTML and JavaScript

This project helps to give an insight into web scraping using BeautifulSoup and Selenium.
It is used for the scraping of websites written in html and java script.

Dataset:
The Dataset contains Employee details as features and predicted attrition as the output. On download, if the csv file is not well formatted (if you are using an older version of MS Excel etc) then use Data -> Text to Column -> select Tab and Comma separation.
